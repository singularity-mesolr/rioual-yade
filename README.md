# rioual-yade

Projet pour Francois rioual, container avec YADE pour le Meso@LR

## BUILD
```bash
singularity build rioual-yade.sif rioual-yade.def
```

## USAGE
```bash
singularity pull rioual-yade.sif oras://registry.forgemia.inra.fr/singularity-mesolr/rioual-yade/rioual-yade:latest
singularity exec rioual-yade.sif yade ...
```
